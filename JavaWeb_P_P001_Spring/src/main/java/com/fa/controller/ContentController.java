package com.fa.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.fa.entities.Content;
import com.fa.entities.Member;
import com.fa.service.ContentService;

@Controller
public class ContentController {

	@Autowired
	private ContentService contentService;

	@RequestMapping(value = "viewContent", method = RequestMethod.GET)
	public String listContent(Model model, @SessionAttribute("memberLogin") Member member) {
		int memberId = member.getMemberId();
		model.addAttribute("content", new Content());
		model.addAttribute("contentList", contentService.getAllContent(memberId));
		return "view-content";
	}

	@RequestMapping(value = "addContent", method = RequestMethod.GET)
	public String addContents(Model model) {
		model.addAttribute("content", new Content());
		return "form-content";
	}

	@RequestMapping(value = "addContent", method = RequestMethod.POST)
	public String addContents(@RequestBody Content content, @SessionAttribute("memberLogin") Member member, Model model) {
		content.setCreateDate(new Date());
		content.setAuthorId(member);		
			contentService.addContent(content);			
			model.addAttribute("message", "Insert successfully!");				
		return "redirect:addContent";
	}

	@RequestMapping(value = "deleteContent", method = RequestMethod.GET)
	public String delContent(@RequestParam(value = "id") int id, Model model,
			@SessionAttribute("memberLogin") Member member) {
		int memberId = member.getMemberId();
		Content content = new Content(id);
		if (contentService.deleteContent(content)) {
			List<Content> contentList = contentService.getAllContent(memberId);
			model.addAttribute("contentList", contentList);
			return "view-content";
		} else {
			return "view-content";
		}
	}

	@RequestMapping(value = "editContent", method = RequestMethod.GET)
	public String editContent(Model model, @RequestParam(value = "idEdit") int idEdit) {
		Content content = contentService.getContentById(idEdit);
		model.addAttribute("content", content);
		return "edit-content";
	}

	@RequestMapping(value = "editContent", method = RequestMethod.POST)
	public String editContent(Content contentUpdate) {
		Content content = contentService.getContentById(contentUpdate.getContentId());
		content.setUpdateDate(new Date());
		content.setTitle(contentUpdate.getTitle());
		content.setBrief(contentUpdate.getBrief());
		content.setContent(contentUpdate.getContent());
		contentService.updateContent(content);
		return "redirect:viewContent";
	}

}


package com.fa.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "Member")
public class Member  {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "memberId")
	private Integer memberId;
	@Column(name = "firstName")
	@Size(max = 30, min = 10, message = "Your firstName must be at least 10 characters long")
	private String firstName;
	@Column(name = "lastName")
	@Size(max = 30, min = 10, message = "Your lastName must be at least 10 characters long")
	private String lastName;
	@Column(name = "username")
	private String userName;	
	@Column(name = "password")
	@Size(max = 30, min = 8, message = "Your password must be at least 9 characters long")
	private String password;
	@Size(max = 13, min = 9, message = "Your phone must be at least 9 characters long")
	@Column(name = "phone")
	private String phone;
	@Email(message = "{member.email.invalid}")
	@Column(name = "email")
	private String email;

	@Column(name = "description")
	private String description;

	@Column(name = "createDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Column(name = "updateDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@OneToMany(mappedBy = "authorId", cascade = CascadeType.ALL)
	private List<Content> contentList;

	public List<Content> getContentList() {
		return contentList;
	}

	public void setContentList(List<Content> contentList) {
		this.contentList = contentList;
	}

	public Member() {
		super();
	}

	public Member(Integer memberId) {
		super();
		this.memberId = memberId;
	}

	public Member(Integer memberId, String firstName, String lastName, String userName, String password, String phone,
			String email, String description, Date createDate, Date updateDate) {
		super();
		this.memberId = memberId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.phone = phone;
		this.email = email;
		this.description = description;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public Member(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public Member(String userName, String password, String email, Date createDate) {
		super();
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.createDate = createDate;
	}

	public Member(String firstName, String lastName, String userName, String password, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.email = email;
	}

	public Member(String firstName, String lastName, String phone, String email, String description, Date updateDate) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
		this.description = description;
		this.updateDate = updateDate;
	}

	/**
	 * @return the memberId
	 */
	public Integer getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	

}

package com.fa.service;

import java.util.List;

import com.fa.entities.Content;

public interface ContentService {
	/**
	 * Get all content
	 * @return list of content
	 */
	List<Content> getAllContent(int memberId);
	
	/**
	 * 
	 * Insert an content
	 * @param content
	 * @return true if insert successfully
	 */
	boolean addContent(Content content);
	
	/**
	 * 
	 * Update content
	 * @param content
	 * @return content after update
	 */
	void updateContent(Content content);

	/**
	 * Delete an content
	 * 
	 * @param content
	 * @return true if delete successfully
	 */
	boolean deleteContent(Content content);
	
	/**
	 * Get content by content id
	 * 
	 * @param contentId
	 * @return content
	 */
	Content getContentById(Integer contentId);
}

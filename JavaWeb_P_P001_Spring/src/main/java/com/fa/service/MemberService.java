package com.fa.service;

import com.fa.entities.Member;

public interface MemberService {
	Member login(String email, String password);

	boolean addMember(Member member);

	Member checkEmail(String email);

	void updateMember(Member member);
}

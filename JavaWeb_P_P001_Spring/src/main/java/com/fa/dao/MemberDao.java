/**
 * 
 */
package com.fa.dao;

import com.fa.entities.Member;

/**
 * @author lamln2
 *
 */
public interface MemberDao {

	Member login(String email, String password);

	boolean addMember(Member member);

	Member checkEmail(String email);

	void updateMember(Member member);
}

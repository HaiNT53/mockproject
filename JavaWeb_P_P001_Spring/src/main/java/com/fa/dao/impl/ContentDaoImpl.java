package com.fa.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fa.dao.ContentDao;
import com.fa.entities.Content;
import com.fa.entities.Member;

@SuppressWarnings("deprecation")
@Repository
public class ContentDaoImpl implements ContentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings({"unchecked" })
	@Override
	public List<Content> getAllContent(int memberId) {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("rawtypes")
		Query query = session.createQuery("FROM Content where authorId =:authorId");
		Member member = new Member(memberId);
		query.setParameter("authorId", member);
		List<Content> contentList = (List<Content>) query.getResultList();
		return contentList;
	}

	@Override
	public Content getContentById(Integer contentId) {
		Session session = sessionFactory.getCurrentSession();
		Content content = session.get(Content.class, contentId);
		return content;
	}

	@Override
	public boolean addContent(Content content) {
		Session session = sessionFactory.getCurrentSession();
		session.save(content);
		return true;
	}

	@Override
	public void updateContent(Content content) {
		Session session = sessionFactory.getCurrentSession();
		session.update(content);
	}

	@Override
	public boolean deleteContent(Content content) {
		Session session = sessionFactory.getCurrentSession();
		if (null != content) {
			session.delete(content);
			return true;
		}
		return false;
	}
}

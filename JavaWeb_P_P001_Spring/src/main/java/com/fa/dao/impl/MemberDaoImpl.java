package com.fa.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fa.dao.MemberDao;
import com.fa.entities.Member;

@SuppressWarnings("deprecation")
@Repository
public class MemberDaoImpl implements MemberDao {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("rawtypes")
	@Override
	public Member login(String email, String password) {
		Member member = null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("From Member where email = :email and password = :password ");
		query.setParameter("email", email);
		query.setParameter("password", password);
		member = (Member) query.uniqueResult();
		return member;
	}

	@Override
	public boolean addMember(Member member) {
		Session session = sessionFactory.getCurrentSession();
		return (session.save(member) != null);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Member checkEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("From Member where email = :email ");
		query.setParameter("email", email);
		Member member = (Member) query.uniqueResult();
		return member;
	}

	@Override
	public void updateMember(Member member) {
		Session session = sessionFactory.getCurrentSession();
		session.update(member);
	}
}

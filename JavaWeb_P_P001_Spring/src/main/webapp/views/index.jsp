<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/menu.css">
	

</head>
<body>
	
	<div class="nav nav-tabs d-flex" id="nav-id">
		<div class="logo mr-auto">
			<a href=" <%=request.getContextPath()%>/index " class="logo-img">CMS</a>
		</div>
		<div class="nav-item dropdown ">
			<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"><i
				class="fa fa-user"></i></a>
			<div class="dropdown-menu">
				<a class="dropdown-item" href="#" id="edit"><i
					class="fa fa-user pr-1"></i> ${memberLogin.userName}</a> <a
					class="dropdown-item" href="login"><i class="fa fa-sign-out"></i>
					Logout</a>
			</div>
		</div>
	</div>
	<!-- menu -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 ">
				<div class="menu">
					<div class="input-search">
						<form action="#">
							<div class="from-inline">
								<input type="text" id="search" name="search"
									placeholder="Search...">
								<button class="submit">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</form>
					</div>
					<div class="content">
						<ul>
							<li class="content_views"><a href="#" id="view"><i
									class="fa fa-th"></i> View contents</a></li>
							<li class="content_from"><a href="#" id="form"><i
									class="fa fa-pencil-square-o"></i> From Contents</a></li>
						</ul>
					</div>

				</div>
			</div>



			<div class="col-md-10 " id="form-content">
				<div id="loader" class="lds-css ng-scope">
					<div class="lds-double-ring">
						<div></div>
						<div></div>
						<div>
							<div></div>
						</div>
						<div>
							<div></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">
		
	</script>
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
		
	<script type="text/javascript"
		src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/index.js"></script>
		
</body>
</html>
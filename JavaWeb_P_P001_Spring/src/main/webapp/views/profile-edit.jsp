<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="info-head">
	<h2>Edit Profile</h2>
</div>
<div class="edit_profile">
	<div class="title">
		<h6>Profile From Element</h6>
	</div>
	<div class="input_info_edit">
		<form:form action="edit" id="profile-edit" method="POST" modelAttribute="member">
			<p id="done">${message}</p>
			<p id="error">${failEdit}</p>
			<div class="form-group">
				<label for="firstname">First Name</label> <form:input type="text" path="firstName"
					class="form-control" id="firstName" name="firstName"
					value="${memberLogin.firstName}"/>
			</div>
			<div class="form-group">
				<label for="lastname">Last Name</label> <form:input type="text" path="lastName"
					class="form-control" id="lastName" name="lastName"
					value="${memberLogin.lastName}"/>
			</div>
			<div class="form-group">
			<label for="eamil">Last Name</label> 
				<h6>${memberLogin.email}</h6>
			</div>
			<div class="form-group">
				<label for="phone">Phone Number</label> <form:input type="text" path="phone"
					class="form-control" id="phone" name="phone"
					value="${memberLogin.phone}"/>
			</div>
			<div class="form-group">
				<label for="description">Description</label>
				<form:textarea name="description" path="description" id="description" class="form-control"
					rows="3" value="${memberLogin.description}"></form:textarea>
			</div>
			<form:button id="btn-Edit">Submit Button</form:button>
			<button type="reset" id="reset">Reset Button</button>
		</form:form>
	</div>
</div>
																
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/validateUpdateProfile.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/updateProfile.js"></script>
	
	

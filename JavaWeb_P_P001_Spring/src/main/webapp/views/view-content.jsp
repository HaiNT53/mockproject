<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>

	<div class="info-head">
		<h2>View Content</h2>
	</div>
	<div class="edit_profile">
		<div class="title">
			<h6>View Content List</h6>
		</div>
		<div class="input_info_edit">

			<table class="table  table-bordered" id="dataTables-example"
				style="width: 100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>Brief</th>
						<th>Content</th>
						<th>Created Date</th>
						<th>Action</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${contentList}" var="content">
						<tr>
							<td>${content.contentId}</td>
							<td>${content.title}</td>
							<td>${content.brief}</td>
							<td>${content.content}</td>
							<td>${content.createDate}</td>
							<td class="d-flex">
								<button type="button"  id="updateContent" class="btn btn-primary mr-4" >
									<i class="fa fa-pencil"></i>									
								</button>
								<button type="button" class="btn btn-danger" id="deleteContent">
									<i class="fa fa-trash"></i>
								</button>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
	</div>
</div>

<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/viewContent.js"></script>
		
		


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/login.css"
	rel="stylesheet">
</head>
<body>

	<div class="login">
		<div class="login_title">
			<h3>Please Sign In </h3>
			<p id="error">${param['error']}</p>
		</div>
				<form:form class="formlogin" id="formlogin" action="index"
					method="POST" name="frm-login" modelAttribute="member">
					<input type="email" class="form-control mb-3" id="email"
						name="email" placeholder="Email address">
					<input type="password" class="form-control" id="password"
						name="password" placeholder="Password">
					<div class="form-group">
						<label class="form-check-label my-2 ml-4"> <input
							class="form-check-input" type="checkbox"> Remember Me
						</label>
					</div>
					<button type="submit"  id="btn-login"
						name="login">Login</button>

					<a href="register" id="link-register"> Click here to Register </a>
				</form:form>
			</div>
	

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/validate-login.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

</body>
</html>
$(document).ready(function() {
	$('#form').click(function() {
		$('#loader').css('display', 'block');
		$('#form-content').html("");
		
		setTimeout(function() {
			$.ajax({
				url : 'addContent',
				type : 'get',
				success : function(data) {
					$('#form-content').html(data);
					$('#loader').css('display', 'none');
				}
			});
		}, 500);
	});


	$('#view').click(function() {
		$('#loader').css('display', 'block');
		$('#form-content').html("");
		
		setTimeout(function() {
			$.ajax({
				url : 'viewContent',
				type : 'get',
				success : function(data) {
					$('#form-content').html(data);
					$('#loader').css('display', 'none');
				}
			});
		}, 500);
	});
	$('#edit').click(function() {
		$('#loader').css('display', 'block');
		$('#form-content').html("");
		
		setTimeout(function() {
			$.ajax({
				url : 'editProfile',
				type : 'get',
				success : function(data) {
					$('#form-content').html(data);
					$('#loader').css('display', 'none');
				}
			});
		}, 500);
	});

	
});
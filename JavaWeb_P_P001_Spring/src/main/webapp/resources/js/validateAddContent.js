  
$(document).ready(function() {
$("#input_info").validate({
        rules: {
            title: {
                required: true,
                rangelength: [10, 200]
            },
            brief: {
                required: true,
                rangelength: [10, 150]
            },
            content: {
                required: true,
                rangelength: [10, 500]
            },
        }
    });
});
$(document).ready(function() {
    $("#formlogin")
   .validate(
	{
		rules : {
			email : "required",
			password : "required",

			password : {
				required : true,
				rangelength : [ 8, 30 ]
			},

			email : {
				required : true,
				rangelength : [ 10, 50 ]
			},

		},
		messages : {

			password : {
				required : 'Enter password',
				minlength : "Your password must be at least 8 characters long",
				equalTo : "Please enter the same password as above"
			},

			email : "Enter  email",

		}
	});
    $("#formregister")
      
    .validate({
        rules: {
        	userName: "required",
            email: "required",
            password: "required",
            confirmPassword: "required",


            password: {
                required: true,
                rangelength: [8,30]
            },
            confirmPassword: {
                required: true,
                rangelength: [8,30],
                equalTo: "#password"
            },
            email: {
                required: true,
                rangelength: [5,50]
            },
            userName: {
                required: true,
                rangelength: [3,30]
            },

        },
        messages: {
        	userName: "Enter user name",
            email: "Enter email",

            password: {
                required: 'Enter password',
                minlength: 'Your password must be at least 8 characters long'
            },
            confirmPassword: {
                required: 'Enter password',
                minlength: 'Your password must be at least 8 characters long',
                equalTo: 'Please enter the same password as above'
            },
            email: {
                required: "Enter email",
                minlength: "Your email must be at least 5 characters long",
               
            },

        }
    });
});
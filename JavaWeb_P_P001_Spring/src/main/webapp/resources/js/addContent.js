
$(document).ready(function() {
$('#btn-addContent').click(function() {
		var title = $('#title').val();
		var brief = $('#brief').val();
		var content = $('#content').val();
		var contentData = {};
		contentData['title'] = title;
		contentData['brief'] = brief;
		contentData['content'] = content;
		if ($('#input_info').valid()) {
			$.ajax({
				url : 'addContent',
				type : 'POST',
				contentType: "application/json",
				data : JSON.stringify(contentData),
				success : function(data) {
					$('#form-content').html(data);
				}
			});
		}

	});
});
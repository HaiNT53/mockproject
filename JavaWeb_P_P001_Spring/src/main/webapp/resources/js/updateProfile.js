$(document).ready(function() {
	/*$("#btn-Edit").click(function() {
		var firstName = $("#firstName").val();
		var lastName = $("#lastName").val();
		var phone = $("#phone").val();
		var email = $("#email").val();
		var description = $("#description").val();
		$.post({
			url : "editProfile",
			data : {
				firstName : firstName,
				lastName : lastName,
				phone : phone,
				email : email,
				description : description
			},
			success : function(response) {
				$("#form-content").html(response);
			}
		});
	});*/
	$('#btn-Edit').click(function() {
		var firstName = $("#firstName").val();
		var lastName = $("#lastName").val();
		var phone = $("#phone").val();
		var email = $("#email").val();
		var description = $("#description").val();
		var contentData = {};
		contentData['firstName'] = firstName;
		contentData['lastName'] = lastName;
		contentData['phone'] = phone;
		contentData['description'] = description;
		
			$.ajax({
				url : 'editProfile',
				type : 'POST',
				contentType: "application/json",
				data : JSON.stringify(contentData),
				success : function(data) {
					$('#form-content').html(data);
				}
			});
		

	});
});